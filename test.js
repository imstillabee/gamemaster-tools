const names = require('./lib/generators/names'),
      race = require('./lib/generators/race'),
      gender = require('./lib/generators/gender'),
      familyName = require('./lib/generators/familyName')

const r = race(),
      g = gender(),
      f = familyName('dwarf')

console.info(r, g, names(r, g), f)
