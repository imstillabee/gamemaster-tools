const random = require('../random'),
      loadText = require('../loadTextArray'),
      madLibs = require('../madLibs'),
      { capitalize, capSecondNotThe } = require('../string-manipulation')


const generateFamilyName = race => {
  let nameArray = loadText(`../data/names/${race}/family.txt`)
  
  let familyName = nameArray[random(nameArray.length) - 1]
  let result = madLibs(familyName, `../data/names/${race}/family`, null, true) // ignore default.txt
  
  result = capitalize(result)
  result = capSecondNotThe(result)
  
  return result
}

module.exports = generateFamilyName
