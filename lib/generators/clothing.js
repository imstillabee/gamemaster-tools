const random = require('../random'),
      loadText = require('../loadTextArray'),
      madLibs = require('../madLibs'),
      { capitalizeSentence, aniffier } = require('../string-manipulation')


const generateFamilyName = (gender = null) => {
  // TODO: establish a per-race clothing
  
  let clothingArray = loadText('../data/clothing/default.txt') // values to randomize from
  
  let description = clothingArray[random(clothingArray.length) - 1] // random seed
  let result = madLibs(description, '../data/clothing', gender)
  
  result = capitalizeSentence(result)
  result = aniffier(result)
  
  return result
}

module.exports = generateFamilyName
