const loadText = require('../loadTextArray')

let kov = require('kov')

let opts = {
  type: 'word',
  min: 3
}

const generateName = (race, gender) => {
  let nameArray = loadText(`../data/names/${race}/${gender}.txt`)
  
  return kov(nameArray, opts)
}

module.exports = generateName
