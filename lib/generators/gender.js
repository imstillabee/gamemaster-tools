const random = require('../random')

const genGender = () => {
  let genderInt = random(10),
      gender = ''
  
  if (genderInt % 2 === 0)
    gender = 'female'
  else
    gender = 'male'
  return gender
}

module.exports = genGender
