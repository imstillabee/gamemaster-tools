const random = require('../random'),
      loadText = require('../loadTextArray')

const raceArray = loadText('../data/names/default.txt')

const generateRace = () => raceArray[random(raceArray.length) - 1]

module.exports = generateRace
