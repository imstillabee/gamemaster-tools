const random = require('../random')

const addNumbersReducer = (a, b) => a + b
const takeHighest = (a, b) => {
  if (a < b)
    return b
  return a
}
const takeLowest = (a, b) => {
  if (a > b)
    return b
  return a
}

const operate = (a, op, b) => {
  switch (op) {
    case '*':
      return a * b
    case '-':
      return a - b
    case '/':
      return a / b
    default:
      return a + b
  }
}

const calculator = (number, faces) => {
  let resultArray = []
  
  if (number < 1)
    number = 1
  if (faces < 1)
    faces = 1
    
  for (let numberDice = 0; numberDice < number; numberDice++)
    resultArray.push(random(faces))
  
  return resultArray
}

// This bad boy does the main work and handles all the flags
const detailed = (number, faces, { advantage = false, disadvantage = false, stat4d6 = false }) => {
  number = parseInt(number, 10)
  faces = parseInt(faces, 10)
  
  if (advantage || disadvantage)
    number += 1
  
  let resultArray = calculator(number, faces),
      total = resultArray.reduce(addNumbersReducer, 0),
      notes
  
  if (advantage) {
    total = resultArray.reduce(takeHighest, 0)
    notes = 'Advantage: took highest number'
  } else if (disadvantage) {
    total = resultArray.reduce(takeLowest, faces)
    notes = 'Disadvantage: took lowest number'
  } else if (stat4d6) {
    const lowestDie = resultArray.reduce(takeLowest)
    
    total -= lowestDie
    notes = `Stat Roll: subtracted the lowest roll [${lowestDie}]`
  }
  
  return { rolls: resultArray, total, notes }
}

// The below functions are basically result formatters
const rawTotal = (number, faces, options) => {
  let result = detailed(number, faces, options)
  
  // TODO: prevent advantage or disadvantage from skeqing the total.
  
  return result.rolls.reduce(addNumbersReducer, 0)
}

const stringDice = (number, faces, options) => {
  let result = detailed(number, faces, options)
  
  return `[${result.rolls.join('] + [')}] = ${result.total}`
}

const algebra = formula => {
  let diceArray = formula.match(/(\d\1+d\d+|\+\1|\-|\*|\/|\d\1+)/gi)
  
  if (diceArray[0].match(/(\+|\-|\/|\*)/g))
    throw new Error('Invalid dice notation.  Cannot begin with an operator.')
  
  let unDicedArray = diceArray.map(operation => {
    if (operation.includes('d')) {
      const dSplitArr = operation.split('d')
  
      return rawTotal(dSplitArr[0], dSplitArr[1], {})
    } else if (!isNaN(operation))
      return parseInt(operation, 10)
    return operation
  })
  
  const mathArray = unDicedArray.slice()
  
  for (let i = 0; i < mathArray.length; i++) {
    if (isNaN(unDicedArray[i])) {
      let threeCalculated = operate(mathArray[i - 1], mathArray[i], mathArray[i + 1])
      
      // this is funky, but replace the third number with the result of all three so it can keep looping
      mathArray[i + 1] = threeCalculated
    }
  }
  
  return mathArray[mathArray.length - 1]
}

module.exports = {
  detailed,
  rawTotal,
  string: stringDice,
  algebra
}
