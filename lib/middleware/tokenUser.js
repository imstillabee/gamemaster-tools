let jwt = require('./login/jwt')

let tokenUser = auth => {
  if (!auth)
    throw Error({ status: 'error', message: 'No token presented.' })
  let token = auth.replace('Bearer ', '')
  
  return jwt.decode(token)
}

let getTokenUser = function (req, res, next) {
  if (req.headers.authorization)
    req.user = tokenUser(req.headers.authorization)
  else
    req.user = {}
  next()
}

module.exports = getTokenUser
