let jwt = require('./jwt')

let tokenUser = auth => {
  if (!auth)
    throw Error({ status: 'error', message: 'No token presented.' })
  let token = auth.replace('Bearer ', '')

  return jwt.decode(token)
}

module.exports = tokenUser
