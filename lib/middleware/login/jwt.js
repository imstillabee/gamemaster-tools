let config = require('../../../config'),
    jwtSimple = require('jwt-simple'),
    userData = require('../../mongo/user')

// IMPORTANT: this function assumes the login is already validated.
let generateJwt = email => new Promise((resolve, reject) => {
  userData.get(null, email)
  .then(user => {
    let currentTime = new Date().getTime() / 1000,
        exipirationTime = config.JWT_EXPIRATION_DAYS * 86400 // 86400 = seconds in a day. 24 * 60 * 60
    //exipirationTime = 1 // text expiration after 1 second

    let payload = {
      _id: user._id,
      _customerId: user._customerId,
      first: user.first,
      last: user.last,
      name: `${user.first} ${user.last}`,
      email: email,
      roles: user.roles,
      exp: currentTime + exipirationTime,
      iat: currentTime
    }

    // format user for saving

    let secret = config.SECRET,
        token = jwtSimple.encode(payload, secret, 'HS512')

    resolve(token)
  })
  .catch(err => reject({ status: 'error', message: 'Could not generate security token', error: err }))
})

let decode = token => {
  let payload = jwtSimple.decode(token, config.SECRET)

  return payload
}

let loginReturnToken = (email, password) => new Promise((resolve, reject) => {
  let userToken = {}

  userData.login(email, password)
  .then(user => {
    userToken.user = user
    return generateJwt(email)
  })
  .then(token => {
    userToken.token = token
    resolve(userToken)
  })
  .catch(err => reject(err))
})


// TODO: Add additional login service calls here

module.exports = {
  loginReturnToken,
  decode,
  generate: generateJwt
}
