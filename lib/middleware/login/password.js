let crypto = require('crypto')

/**
 * generates random string of characters i.e salt
 * @function
 * @param {number} length - Length of the random string.
 */

let genRandomString = length =>
  crypto.randomBytes(Math.ceil(length / 2))
  .toString('hex') // convert to hexadecimal format */
  .slice(0, length)  // return required number of characters */

/**
 * hash password with sha512.
 * @function
 * @param {string} password - List of required fields.
 * @param {string} salt - Data to be validated.
 */
let sha512 = (password, salt) => {
  let hash = crypto.createHmac('sha512', salt) // ** Hashing algorithm sha512 */

  hash.update(password)

  let value = hash.digest('hex')

  return {
    salt: salt,
    hash: value
  }
}

let saltHashPassword = password => {
  let salt = genRandomString(32) // ** Gives us salt of length 32 */

  // returns { hash, salt }
  return sha512(password, salt)
}

let reHashWithSalt = (salt, password) => sha512(password, salt).hash

module.exports = {
  generateSaltAndHash: saltHashPassword,
  reHashWithSalt
}
