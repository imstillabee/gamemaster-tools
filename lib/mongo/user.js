let mongoose = require('../../models/index').mongoose,
    Promise = require('bluebird'),
    pwd = require('../middleware/login/password'),
    jwtSimple = require('jwt-simple'),
    config = require('../../config')

// Setup Models
let docDetails = require('../../models/doc-details'),
    decP = require('./deconstruct-properties')

let Users = mongoose.model('Users')

let getUser = (userId, email) =>
  new Promise((resolve, reject) => {
    let query = {}
    
    if (userId) {
      query._id = userId
    } else if (email) {
      query.email = email
    }
    
    Users.findOne(query, (err, user) => {
      if (err) {
        return reject(err)
      }
      resolve(user)
    })
  })

let getAvatarAndName = email =>
  new Promise((resolve, reject) => {
    if (!email) {
      reject({ status: 'error', message: 'Avatar Garbage in = Garbage out' })
    }
    
    let query = { email }
    
    Users.findOne(query, 'first last photoURI', (err, user) => {
      if (err) {
        return reject(err)
      }
      if (user) {
        resolve(user.toObject())
      } else {
        reject({ status: 'error', message: 'Avatar User Garbage in = Garbage out' })
      }
    })
  })

let getFromToken = token =>
  new Promise((resolve, reject) => {
    if (!token) {
      reject({ status: 'error', message: 'Token Garbage in = Garbage out' })
    }
    
    let tokenUser = jwtSimple.decode(token, config.SECRET)
    
    let query = { _id: tokenUser._id }
    
    Users.findOne(query, '-password -salt', (err, user) => {
      if (err) {
        return reject(err)
      }
      if (user) {
        resolve(user.toObject())
      } else {
        reject({ status: 'error', message: 'Token User Garbage in = Garbage out' })
      }
    })
  })

let loginUser = (email, password) =>
  new Promise((resolve, reject) => {
    let query = {
      email
    }
    
    // TODO: add validation against passwordExpiry if it exists.
    Users.findOne(query, (err, user) => {
      if (err) return reject(err)
      if (user && user.salt) {
        query = {
          email,
          password: pwd.reHashWithSalt(user.salt, password)
        }
        Users.findOne(query, '-password -salt', (err, validUser) => {
          if (err) return reject(err)
          if (validUser) {
            resolve(validUser.toObject())
          } else {
            reject({ status: 'error', message: 'Invalid login.' })
          }
        })
      } else {
        reject({ status: 'error', message: 'Invalid login.' })
      }
    })
  })

let createUser = (creatingUser, userObject) =>
  new Promise((resolve, reject) => {
    userObject.docDetails = docDetails
    userObject.docDetails.userCreated = creatingUser
    
    if (!userObject.password) {
      return reject({ message: 'No password supplied' })
    }
    // prevent certain values from being set
    delete creatingUser.salt
    
    // get the salt and hashed password
    let saltHash = pwd.generateSaltAndHash(userObject.password)
    
    userObject.salt = saltHash.salt
    userObject.password = saltHash.hash
    
    let newRecord = new Users(userObject)
    
    newRecord.save()
    .then(record => resolve(record))
    .catch(err => reject(err))
  })

let updateUser = (userId, updatingUser, userData) =>
  new Promise((resolve, reject) => {
    let dt = new Date().getTime()
    
    Users.findOne({ _id: userId }, (err, user) => {
      if (err) {
        reject(err)
        return
      }
      
      // prevent attempts to update naughty bits
      delete userData._customerId
      delete userData._Id
      
      if (userData.password) {
        // get the salt and hashed password
        let saltHash = pwd.generateSaltAndHash(userData.password)
        
        userData.salt = saltHash.salt
        userData.password = saltHash.hash
      }
      
      let propArr = decP(userData)
      
      propArr.forEach(valuePair => {
        if (typeof valuePair[1] === 'boolean' || valuePair[1]) // TODO: Ensure false is good.
        {
          user[valuePair[0]] = valuePair[1]
        }
      })
      
      user.docDetails.dateModified = dt
      user.docDetails.userModified = updatingUser
      user.save()
      .then(record => resolve(record))
      .catch(err => reject(err))
    })
  })

// status: ['Active', 'Inactive', 'Suspended for Payment', 'Suspended']
let deactivateUser = (id, updatingUser) =>
  new Promise((resolve, reject) => {
    updateUser(id, updatingUser, { isActive: false })
    .then(record => resolve(record))
    .catch(err => reject(err))
  })

let flagDeleted = (updatingUser, id) =>
  new Promise((resolve, reject) => {
    updateUser(id, updatingUser, { isDeleted: true })
    .then(record => resolve(record))
    .catch(err => reject(err))
  })

module.exports = {
  get: getUser,
  create: createUser,
  flagDeleted,
  update: updateUser,
  deActivate: deactivateUser,
  login: loginUser,
  avatar: getAvatarAndName,
  getFromToken: getFromToken
}
