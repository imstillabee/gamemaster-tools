let deconstruct = obj => {
  let retArr = []
  
  for (let name in obj) {
    if (obj.hasOwnProperty(name))
      retArr.push([name, obj[name]])
  }
  
  return retArr
}

module.exports = deconstruct
