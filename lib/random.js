const rndIntMax = max => {
  max = Math.floor(max)
  return Math.floor(Math.random() * max) + 1
}

module.exports = rndIntMax
