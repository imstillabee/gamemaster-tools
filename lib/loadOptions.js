const fs = require('fs'),
      path = require('path')


const loadVariableOptions = (sourceDir, gender, ignoreDefault = false) => {
  let defaultPath = sourceDir
  
  if (!ignoreDefault)
    defaultPath = path.join(sourceDir, 'default.txt')
  let defaultValues = '',
      optionValues = '',
      joinedValues = ''
  
  if (fs.existsSync(defaultPath))
    defaultValues = fs.readFileSync(defaultPath, 'utf8')
  
  if (gender) {
    const optionPath = path.join(sourceDir, `${gender}.txt`)
    
    if (fs.existsSync(optionPath))
      optionValues = fs.readFileSync(optionPath, 'utf8')
  
    joinedValues = `${defaultValues}\n${optionValues}\n`
  } else
    joinedValues = defaultValues
  
  if (!joinedValues || joinedValues === '')
    joinedValues = '\n'
  let valuesArray = joinedValues.split('\n').filter(value => value !== '')
  
  for (let idx = valuesArray.length - 1; idx >= 0; idx--) {
    let line = valuesArray[idx]
    const setting = line.match(/# ?(.*):\s?(\w+%?)\s?(.*)/) || ['', ''],
          action = setting[1].toLowerCase()
    let actionValue = setting[2],
        value = setting[3] || ''
    
    switch (action) {
      case 'blanks': {
        let blankCount = actionValue
  
        valuesArray.splice(idx, 1) // remove this one
  
        if (blankCount.includes('%')) {
          let startingLines = valuesArray.length,
              percent = parseInt(blankCount, 10)
    
          blankCount = startingLines / (1 - percent / 100) - startingLines
        }
        for (let i = 0; i < Math.floor(blankCount); i++)
          valuesArray.push('')
        break
      }
      case 'repeat': {
        valuesArray.splice(idx, 1) // remove this one
        for (let i = 0; i < Math.floor(actionValue); i++)
          valuesArray.push(value)
        break
      }
      default:
        break
    }
  }
  return valuesArray
}

module.exports = loadVariableOptions
