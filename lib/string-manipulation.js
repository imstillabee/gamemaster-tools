const capitalize = text => text.replace(/(\w)(.*)/, (text, m1, m2) => `${m1.toUpperCase()}${m2}`)
const capSecondNotThe = text => text.replace(/(the|The)\s(\w)(.*)/, (text, m1, m2, m3) => `${m1.toLowerCase()} ${m2.toUpperCase()}${m3}`)
const capitalizeSentence = text => {
  let sentenceArr = text.split('.').map(s => s.trim())
  
  return sentenceArr.map(s => `${s.substr(0, 1).toUpperCase()}${s.substr(1)}.`).join(' ')
}
const aniffier = text => text.replace(/\sa\s([aeiou])/ig, (text, voul) => ` an ${voul}`)

module.exports = {
  capitalize,
  capSecondNotThe,
  capitalizeSentence,
  aniffier
}
