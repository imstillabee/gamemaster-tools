const fs = require('fs'),
      path = require('path')

const loadOptions = require('./loadOptions')

const isDir = source => {
  if (!fs.existsSync(source)) return false
  return fs.lstatSync(source).isDirectory()
}

const loadCategory = (filePath, ignoreDefault = false) => {
  let subDirArray = fs.readdirSync(filePath).filter(fileOrDir => isDir(path.join(filePath, fileOrDir)))
  
  let response = {}
  
  subDirArray.forEach(dir => {
    let subDirs = fs.readdirSync(path.join(filePath, dir))
  
    subDirs = subDirs.filter(fileOrDir => isDir(path.join(filePath, dir, fileOrDir)))
  
    response[dir] = loadOptions(path.join(filePath, dir), null, ignoreDefault)
    if (subDirs.length > 0) {
      subDirs.forEach(subDir => {
        response[`${dir}-${subDir}`] = loadOptions(path.join(filePath, dir, subDir), null, ignoreDefault) // TODO: could pass alternate text file as well as default.txt
      })
    }
  })
  
  let fileArray = fs.readdirSync(filePath).filter(fileOrDir => !isDir(path.join(filePath, fileOrDir)))
  
  fileArray.forEach(file => {
    let className = file.replace(/\.txt/, '')
    
    response[className] = (response[file] || []).concat(loadOptions(path.join(filePath, file), 'male', true))
  })
  
  return response
}

module.exports = loadCategory
