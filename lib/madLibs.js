const loadCategory = require('./loadCategory'),
      path = require('path'),
      random = require('./random')

const replaceVars = (source, matchesArray, varValues) => {
  if (!matchesArray)
    return source
  matchesArray.forEach(varToReplace => {
    const subObj = varValues[varToReplace.substr(1).replace('~', '')]
    
    if (!subObj) {
      console.info(source)
      throw Error(`\n\nMISSING DATA ARRAY: ${varToReplace}\n\n`)
    }
    let foundValue = subObj[random(subObj.length) - 1],
        regex = new RegExp(`\\${varToReplace}`)
    
    source = source.replace(regex, foundValue)
  })
  return source
}

const findVariables = (source, options) => {
  if (!source)
    return
  // find all the variables
  let matches = source.match(/(\$[a-zA-Z-]+~?)/g)
  
  // but only the unique ones
  // matches = matches.filter((item, pos) => matches.indexOf(item) === pos)
  // console.info(matches)
  // replace them
  source = replaceVars(source, matches, options)
  
  if (source.includes('$'))
    source = findVariables(source, options)
  
  return source
}

const cleanSentenses = text => {
  text = text.replace(/\s\s/g, ' ')
  
  return text
}

const madLibs = (text, filePath, gender, ignoreDefault = false) => {
  let optionsObject = loadCategory(path.join(__dirname, filePath), ignoreDefault),
      generalOptions = loadCategory(path.join(__dirname, '../data/general'), ignoreDefault)
  
  optionsObject = Object.assign(optionsObject, generalOptions)
  
  let his = 'his',
      he = 'he'
  
  switch (gender) {
    case 'female':
      his = 'her'
      he = 'she'
      break
    case 'it':
      his = 'its'
      he = 'it'
      break
    default:
      break
  }
  
  // Force the option for gender to be the selected one
  optionsObject['his-her'] = [his]
  optionsObject['he-she'] = [he]
  
  let prepped = findVariables(text, optionsObject)
  
  return cleanSentenses(prepped)
}

module.exports = madLibs
