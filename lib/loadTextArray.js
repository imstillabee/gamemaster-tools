const fs = require('fs'),
      path = require('path')

const load = filePath => fs.readFileSync(path.join(__dirname, filePath)).toString().split('\n').filter(el => el.length != 0)

module.exports = load
