// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// nameGenerator.js
// written and released to the public domain by drow <drow@bin.sh>
// http://creativecommons.org/publicdomain/zero/1.0/

let nameSet = {}
let chainCache = {}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// generator function

function generateName (type) {
  let chainGenName = markovChain(type)
  
  if (chainGenName)
    return markovName(chainGenName)
  
  return ''
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// generate multiple

function nameList (type, nOf) {
  let list = []
  let i
  
  for (i = 0; i < nOf; i++)
    list.push(generateName(type))
  
  return list
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// get markov chain by type

function markovChain (type) {
  let markocChainChain = chainCache[type]
  
  if (markocChainChain)
    return markocChainChain
  
  let list = nameSet[type]
  
  if (list) {
    let chain = constructChain(list)
    
    if (chain) {
      chainCache[type] = chain
      return chain
    }
  }
  return false
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// construct markov chain from list of names

function constructChain (list) {
  let chain = {}
  
  for (let i = 0; i < list.length; i++) {
    let names = list[i].split(/\s+/)
  
    chain = incrChain(chain, 'parts', names.length)
    
    for (let j = 0; j < names.length; j++) {
      let name = names[j]
      
      chain = incrChain(chain, 'nameLen', name.length)
      
      let c = name.substr(0, 1)
      
      chain = incrChain(chain, 'initial', c)
      
      let string = name.substr(1)
      let lastC = c
      
      while (string.length > 0) {
        let cChar = string.substr(0, 1)
        
        chain = incrChain(chain, lastC, cChar)
        
        string = string.substr(1)
        lastC = cChar
      }
    }
  }
  return scaleChain(chain)
}

function incrChain (chain, key, token) {
  if (chain[key]) {
    if (chain[key][token])
      chain[key][token]++
    else
      chain[key][token] = 1
  } else {
    chain[key] = {}
    chain[key][token] = 1
  }
  return chain
}

function scaleChain (chain) {
  let tableLen = {}
  let key
  
  for (key in chain) {
    tableLen[key] = 0
    
    let token
    
    for (token in chain[key]) {
      let count = chain[key][token]
      let weighted = Math.floor(Math.pow(count, 1.3))
      
      chain[key][token] = weighted
      tableLen[key] += weighted
    }
  }
  chain.tableLen = tableLen
  return chain
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// construct name from markov chain

function markovName (chain) {
  let parts = selectLink(chain, 'parts')
  let names = []
  
  for (let i = 0; i < parts; i++) {
    let nameLen = selectLink(chain, 'nameLen')
    let c = selectLink(chain, 'initial')
    let name = c
    let lastC = c
    
    while (name.length < nameLen) {
      c = selectLink(chain, lastC)
      name += c
      lastC = c
    }
    names.push(name)
  }
  return names.join(' ')
}

function selectLink (chain, key) {
  let len = chain.tableLen[key]
  let idx = Math.floor(Math.random() * len)
  
  let t = 0
  
  for (let token in chain[key]) {
    t += chain[key][token]
    if (idx < t)
      return token
  }
  return '-'
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
