# GM-Toolbox API

Provides GMs with an easy to use API for generating a lot of _things_; and GM's love _things_.

## Data Structure

There are multiple types of files in the data structure, all of which follow the same constraints and parameters.
All files are simple, new-line delimited, `.txt` files.

All entries within files can contain variables which are delimited with a dollar sign.
Dashes can be used within variable names for readability.  **Note:** _This means that if you want a variable to connect with either another variable or a static value, you cannot.
In order to achieve "[color]-face"_ where "[color]" is a variable and the rest static, you must use a designated replacement, the tilde (~).
Therefore "$color~face" might output "blue-face"

#### Example Variables
|Variable|Example output|
|:----|:----|
|`$color`|red|
|`$wood`|oak|
|`$color$wood`|greenpine|
|`$body-part`|belly|
|`$color~$armor`|blue-shield, red-helm|


### File Types
|File Type|Example File|Definition|
|:----|:----|:----|
|source files|`dwarf/family.txt`|These files hold the "MadLibs" source elements for string building|
|variable files|`element.txt`|These files hold lists of variables.  These files can also contain $variables|
|markov chain files|`dwarf/male.txt`|These files hold lists of known valid items (in this case Dwarven male names).  These are in turn consumed by a markov chain function and a new random name matching the patterns identified is output. _(These files are not externally different than the variable files, but rather are consumed by the engine differently based on the path pattern, namely, `./data/names`_|

#### Source Files
Files contain a list of new-line delimited items with variables on each line

_Example:_ `./dwarf/family.txt`
```
$adj-good$body-part
$adj-good$element
$color$element
$color$weapon
$color~armor
```

#### Variable Files
Files are named in the singular form and contain a list of new-line delimited items.

_Example:_ `element.txt`
```
earth
wind
fire
ice
water
```

#### Markov Chain Files (Name files)
Files are named in the singular form and contain a list of new-line delimited items.

_Example:_ `./dwarf/male.txt`
```
Kíli
Lóni
Mîm
Narvi
Nori
Náin
Náli
Nár
Ori
```

### _Notes on files_
1. Blank lines on data files are ignored.
2. Variables are processed recursively, so it is possible to screw up your names and time yourself out by having values which only refer to themselves, for example a `color.txt` which only has values which also contain `$color...` as it will loop infinately until it reaches the program's max loop count.


### Data Structure

| Folder | Description |
|:----|:----|
|`/data`| Holds folders of data items |
|`../general`| Holds text files with generic lists. These are loaded seperately and used across all sub-folders|
|`../names`|Holds race-name sub-folders as well as a `default.txt` with a list of active races and probabilities|
|`../../dwarf`|Contains 3 prescribed files (per race): `male.txt, female.txt, family.txt`|
|`../../../family`|Contains variable files specific to dwarven family names for use in MadLibs building|
|`../../../male.txt`|A markov file listing valid male dwarven names|
|`../../../female.txt`|A markov file listing valid female dwarven names|
|`../../../family.txt`|A source file for building dwarven family names with MadLibs|
|`../../elf`|...|
|**`../[OTHER CATEGORY]`**| Holds a `default.txt` which is the source file for that category|
|`../../[variable_folder]`|Holds three files and possibly sub-folders. `default.txt` holds non-gender specific items. `male.txt` items are added to the list if the subject is male. `female.txt` is added to the list if subject is female. `it.txt` items are added to the list if the subject has no gender (this can be dropped if none of the items rely on gender).|


## Advanced List Usage
Variable Files can contain modifier text as well. This text allows for modifying the percentage chance you'd get a particular value.
It does this in two ways.

### Blank values
At times you will want a variable to have a percentage chance of coming back blank. In order to achieve this, begin _the first line_ of your variable file with:
`# Blanks: [percentage]%`

#### Example
We want some characters to have a ring or multiple rings in a description, but not everyone has rings, nor should it be an equal percentage chance that they will have rings.
```
# Blanks: 70%
a ring
a couple rings
```

### Higher percentage chances
Other times you will want an existing value to have a higher chance to occur than `1:[line count]`, for those times there is a repeat directive.
`# Repeat: [number] [normal variable value]`

### Example
We want our single ring option to be a 4/5 chance if the person has a ring at all.  Therefore we will modify the above example as follows:

```
# Blanks: 70%
# Repeat: 4 a ring
a couple rings
```
This has the effect of performing the following calculations:

1. There is a 70% chance a blank value would be returned
2. There is therefore a 30% chance a non-blank will return.
3. The repeat directive will in this case quadrouple the chanes of having "a ring" over having "a couple rings"

This is the equivalent of having:
```
... about 12 blank lines followed by ...
a ring
a ring
a ring
a ring
a couple rings
```
