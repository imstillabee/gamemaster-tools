let router = require('express').Router()

let diceRoute = require('./dice')

router.use('/dice', diceRoute)

module.exports = router
