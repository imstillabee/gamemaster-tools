let router = require('express').Router()

let diceLib = require('../lib/dice/dice')

/* GET dice listing. */
router.get('/raw/:dice/:faces/:options?', (req, res) => {
  let { dice, faces, options } = req.params
  
  let opt = {}
  
  if (options)
    opt[options] = true
  
  res.status(200).json(diceLib.rawTotal(dice, faces, opt))
})

router.get('/detailed/:dice/:faces/:options?', (req, res) => {
  let { dice, faces, options } = req.params

  let opt = {}

  if (options)
    opt[options] = true

  res.status(200).json(diceLib.detailed(dice, faces, opt))
})

router.get('/string/:dice/:faces/:options?', (req, res) => {
  let { dice, faces, options } = req.params

  let opt = {}

  if (options)
    opt[options] = true

  res.status(200).json(diceLib.string(dice, faces, opt))
})

router.get('/:formula', (req, res) => {
  let { formula } = req.params

  res.status(200).json(diceLib.algebra(formula))
})

module.exports = router
