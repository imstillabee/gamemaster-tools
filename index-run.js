const names = require('./lib/generators/names'),
      race = require('./lib/generators/race'),
      gender = require('./lib/generators/gender'),
      familyName = require('./lib/generators/familyName')

const r = race()
console.info(r)
const g = gender()
console.info(g)
const f = familyName(r)
console.info(f)

console.info(r, g, names(r, g), f)
