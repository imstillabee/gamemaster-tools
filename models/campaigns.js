let mongoose = require('mongoose'),
    docDetailsSchema = require('./doc-details'),
    npcSchema = require('./npcs')

let schema = mongoose.Schema

let campaignSchema = schema({
  _userEmail: { type: String, required: true },
  name: { type: String, required: true },
  shortName: String,
  color: { type: String, 'default': '#2196f3' },
  npc: [ npcSchema ],
  isActive: { type: Boolean, 'default': true },
  docDetails: docDetailsSchema
})

mongoose.model('Campaigns', campaignSchema)
