let mongoose = require('mongoose'),
    docDetailsSchema = require('./doc-details')

let schema = mongoose.Schema

const statBlock = {
  str: Number,
  dex: Number,
  con: Number,
  'int': Number,
  wis: Number,
  cha: Number,
  ac: Number,
  hp: Number,
  speed: Number,
  abilities: [ String ],
  cr: Number,
  exp: Number
}

const action = {
  name: { type: String, required: true },
  toHit: Number,
  reach: { type: Number, 'default': 5 },
  rangeMin: Number,
  rangeMax: Number,
  damage: String,
  damageType: String
}

let npcSchema = schema({
  name: { type: String, required: true },
  race: { type: String, required: true },
  appearance: { type: String },
  voice: { type: String },
  motivations: { type: String },
  profession: { type: String },
  stats: statBlock,
  actions: [ action ],
  isActive: { type: Boolean, 'default': true },
  docDetails: docDetailsSchema
})

mongoose.model('Npcs', npcSchema)
