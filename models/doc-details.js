let mongoose = require('mongoose')

let schema = mongoose.Schema

let docDetailsSchema = schema({
  dateCreated: { type: Date, 'default': Date.now },
  dateModified: { type: Date, 'default': Date.now },
  userCreated: String,
  userModified: String
})

module.exports = docDetailsSchema
