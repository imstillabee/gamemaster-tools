let mongoose = require('mongoose'),
    docDetailsSchema = require('./doc-details')

let schema = mongoose.Schema

let userSchema = schema({
  email: { type: String, lowercase: true, trim: true, required: true },   // email address
  first: { type: String, required: true },
  last: String,
  title: String,
  password: { type: String, required: true },
  salt: { type: String, required: true },
  photoURI: String,
  isActive: { type: Boolean, 'default': true },
  isDeleted: { type: Boolean, 'default': false },
  docDetails: docDetailsSchema
})

mongoose.model('Users', userSchema)
