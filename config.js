let veil = require('./veil.json')

let getDbSecret = (server, user) => {
  let serverList = veil.accounts.filter(o => o.label === server)
  
  if (serverList.length === 0) throw Error(`Configuration setup could not find an entry for ${server}`)
  
  let account = serverList.filter(o => o.user === user)
  
  if (account.length === 0) throw Error(`Configuration setup could not find an entry for user ${user}`)
  
  return account
}

let codeAmbiance = process.env.CODE_TARGET || 'DEV'

let config = {
  ENVIRONMENT: codeAmbiance,
  JWT_EXPIRATION_DAYS: 45,
  SECRET: veil.jwtSecret,
  DEV: {
    MONGO: {
      URI: 'gm-toolbox-dev',  // Locally this should be a host file pointing to localhost
      OPTIONS: {
        user: 'gm-toolbox',
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000,
        keepAlive: 300000,
        connectTimeoutMS: 18000000
      },
      useCredentials: true
    }
  }
}

config.MONGO = config[config.ENVIRONMENT].MONGO

// Mongo DB Settings

let m = config.MONGO,
    mo = m.OPTIONS

if (m.useCredentials === true) {
  console.log('~.~.~ Pulling credentials for [' + m.URI + '] ~.~.~')
  let cred = getDbSecret(m.URI, mo.user)[0]
  
  mo.user = cred.user
  mo.pass = cred.pass
  mo.auth = {}
  mo.auth.authdb = cred.authdb
  m.SERVER = cred.server
}

module.exports = config
