require('chai').should()

const dice = require('../lib/dice/dice')

const dieTypeArray = [ 4, 6, 8, 10, 12, 20, 100 ]

describe('Rolls the dice...all night long', () => {
  dieTypeArray.forEach(faces =>
    it(`rolls a d${faces}`, () => {
      const result = dice.detailed(1, faces, {})
  
      result.total.should.be.above(0)
      result.total.should.be.below(faces + 1)
    })
  )
  dieTypeArray.forEach(faces =>
    it(`rolls 4d${faces}`, () => {
      const dieCount = 4
      const result = dice.detailed(dieCount, faces, {})
      
      result.rolls.forEach(roll => {
        roll.should.be.above(0)
        roll.should.be.below(faces + 1)
      })
      result.total.should.be.above(dieCount - 1)
      result.total.should.be.below(faces * dieCount + 1)
    })
  )
  it('rolls a d20 with advantage', () => {
    const faces = 20
    const result = dice.detailed(1, faces, { advantage: true })
  
    result.rolls.length.should.be.greaterThan(1)
    let biggerNumber = result.rolls[0]
    
    if (biggerNumber < result.rolls[1])
      biggerNumber = result.rolls[1]
    
    result.total.should.equal(biggerNumber)
  })
  it('rolls a d20 with disadvantage', () => {
    const faces = 20
    const result = dice.detailed(1, faces, { disadvantage: true })
    
    result.rolls.length.should.be.greaterThan(1)
    let smallerNumber = result.rolls[0]
    
    if (smallerNumber > result.rolls[1])
      smallerNumber = result.rolls[1]
    
    result.total.should.equal(smallerNumber)
  })
  it('rolls 4d6 for stats, drops the lowest die', () => {
    const faces = 6
    const result = dice.detailed(4, faces, { stat4d6: true })
    
    result.rolls.length.should.be.greaterThan(3)
    let calcTotal = 0,
        smallestDie = faces
    
    for (let iDie = 0; iDie < 4; iDie++) {
      const thisDie = result.rolls[iDie]
      
      calcTotal += thisDie
      if (smallestDie > thisDie)
        smallestDie = thisDie
    }
    
    result.total.should.equal(calcTotal - smallestDie)
  })
  it('Rolls dice and only returns the total', () => {
    const dieCount = 10,
          faces = 10,
          result = dice.rawTotal(dieCount, faces, {})
    
    result.should.be.a('number')
  })
  it('Rolls dice and returns a formatted string', () => {
    const dieCount = 10,
          faces = 10,
          result = dice.string(dieCount, faces, {})
    
    result.should.be.a('string')
  })
  const goodDiceNotationTests = [
    { notation: '1d4 + 2d10 -d8 + 3', min: -2, max: 19 },
    { notation: '3d8 + 3', min: 6, max: 27 },
    { notation: '1d4 *2', min: 2, max: 8 }
  ]
  
  goodDiceNotationTests.forEach(d =>
    it(`does a little die algebra "${d.notation}"`, () => {
      const result = dice.algebra(d.notation)
      
      result.should.be.greaterThan(d.min - 1)
      result.should.be.lessThan(d.max + 1)
    })
  )
  
  const badDiceNotationTests = [
    { notation: '-7 + d5', min: -6, max: -2 }
  ]
  
  badDiceNotationTests.forEach(d =>
    it(`does a little die algebra "${d.notation}"`, () => {
      try {
        dice.algebra(d.notation)
      } catch (err) {
        err.should.exist
      }
    })
  )
})
