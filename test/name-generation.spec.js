const fs = require('fs'),
      path = require('path')

require('chai').should()

const names = require('../lib/generators/names')
const familyName = require('../lib/generators/familyName')

let raceArray = []

// Pull the available race names
let racePath = path.join(__dirname, '../data', '/names')
let dirArray = fs.readdirSync(racePath)

raceArray = dirArray.filter(item => fs.lstatSync(path.join(racePath, item)).isDirectory() === true)

describe('Generates names for each race and gender', () => {
  raceArray.forEach(race =>
    it(`Generates a male name for a ${race}`, () => {
      const name = names(race, 'male')

      console.info('male', race, name)
      name.should.be.a('string')
      name.length.should.be.greaterThan(1)
    })
  )
  raceArray.forEach(race =>
    it(`Generates a female name for a ${race}`, () => {
      const name = names(race, 'female')
      
      console.info('female', race, name)
      name.should.be.a('string')
      name.length.should.be.greaterThan(1)
    })
  )
  raceArray.forEach(race =>
    it(`Generates a family name for a ${race}`, () => {
      const name = familyName(race)
  
      console.info('family', race, name)
      name.should.be.a('string')
      name.should.not.include('undefined')
      name.length.should.be.greaterThan(1)
    })
  )
})
