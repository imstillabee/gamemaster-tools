const fs = require('fs'),
      path = require('path')

require('chai').should()

const gender = require('../lib/generators/gender'),
      race = require('../lib/generators/race')

const filterOnlyUnique = (value, index, self) => self.indexOf(value) === index

let raceArray = []

// Pull the available race names
let racePath = path.join(__dirname, '../data', '/names')
let dirArray = fs.readdirSync(racePath)

raceArray = dirArray.filter(item => fs.lstatSync(path.join(racePath, item)).isDirectory() === true)

describe('Randomly Pick Values', () => {
  it('picks a random gender', () => {
    let randomValues = []
    
    for (let i = 0; i < 10; i++)
      randomValues.push(gender())
    
    const uniqueValuesArray = randomValues.filter(filterOnlyUnique)
    
    uniqueValuesArray.length.should.be.equal(2)
  })
  
  it('picks a random race', () => {
    let randomValues = []
    
    for (let i = 0; i < 10; i++)
      randomValues.push(race())
    
    const uniqueValuesArray = randomValues.filter(filterOnlyUnique)
    
    uniqueValuesArray.length.should.be.greaterThan(raceArray.length * 0.75)
  })
})
