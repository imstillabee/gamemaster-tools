let express = require('express'),
    hbs = require('hbs'),
    debug = require('debug'),
    cors = require('cors'),
    path = require('path'),
    favicon = require('serve-favicon'),
    expressJwt = require('express-jwt'),
    logger = require('morgan'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser'),
    db = require('./models'),
    assignTokenInfoToRequest = require('./lib/middleware/tokenUser'),
    config = require('./config')

let app = express()

db.init(config.MONGO.SERVER, config.MONGO.OPTIONS)

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(logger('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cookieParser())
// app.use(require('node-sass-middleware')({
//   src: path.join(__dirname, 'public'),
//   dest: path.join(__dirname, 'public'),
//   indentedSyntax: true,
//   sourceMap: true
// }))
app.use(express.static(path.join(__dirname, 'public')))

app.use(cors())

// app.use(expressJwt({ secret: config.SECRET })
// .unless({
//   path: [
//     '/',
//     /\/dice\/.*/
//   ]
// })
// )

app.use(assignTokenInfoToRequest)

/*
 * Route Mapping
 */

let routes = require('./routes')

app.use('/', routes)

// catch 404 and forward to error handler
app.use((req, res, next) => {
  let err = new Error('Not Found')
  
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res, next) => {
    
    res.status(err.status || 500)
    res.json({
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use((err, req, res, next) => {
  res.status(err.status || 500)
  res.render('error', {
    message: err.message,
    error: {}
  })
})

module.exports = app
